#!/usr/bin/env cabal
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{- cabal:
build-depends: base, aeson >= 2 , containers, bytestring
-}

import Data.Coerce
import Data.String (String)
import Data.Aeson as A
import qualified Data.Map as Map
import Data.Map (Map)
import qualified Data.ByteString.Lazy as B hiding (putStrLn)
import qualified Data.ByteString.Lazy.Char8 as B
import Data.List (intercalate)
import Data.Set (Set)
import qualified Data.Set as S
import System.Environment
import Data.Maybe

-----------------------------------------------------------------------------
-- Definition of a BuildConfig (options which affect the binaries which are in the bindist)
-----------------------------------------------------------------------------

-- | Operating system
data Opsys
  = Linux LinuxDistro
  | Darwin DarwinToolchain
  | FreeBSD13
  | Windows deriving (Eq)

data DarwinToolchain = System | Brew deriving Eq

data Linker = Default | NoOverride | Bfd | Gold | Lld | Mold deriving Eq

data AlpineVersion = V3_12 | V3_18 | V3_20 deriving Eq

data LinuxDistro
  = Debian12 | Debian11 (Maybe LlvmVersion) | Debian10 Linker | Debian9 | Fedora33 | Ubuntu2404 | Ubuntu2004 | Ubuntu2204 | Ubuntu1804 | Centos7 | Alpine AlpineVersion Linker
    | ArchLinux | Fedora27 | Fedora36 | Fedora38 | Fedora39 Linker | Fedora40 | LinuxMint19 | LinuxMint20 | LinuxMint21 deriving (Eq)

data Arch = Amd64 | AArch64 | I386

data BignumBackend = Native | Gmp deriving Eq

newtype LlvmVersion = LlvmVersion { llvmVersion :: Int } deriving Eq

supportedLlvmVersions :: [LlvmVersion]
supportedLlvmVersions = [ LlvmVersion n | n <- [13..18] ]

llvm15 = LlvmVersion 15



-----------------------------------------------------------------------------
-- Platform specific variables
-----------------------------------------------------------------------------

-- | These tags have to match what we call the runners on gitlab
runnerTag :: Arch -> Opsys -> String
runnerTag arch (Linux distro) =
  case arch of
    Amd64   -> "x86_64-linux"
    AArch64 -> "aarch64-linux"
    I386    -> "x86_64-linux"
runnerTag AArch64 (Darwin {}) = "aarch64-darwin"
runnerTag Amd64 (Darwin {}) = "x86_64-darwin-m1"
runnerTag Amd64 Windows = "new-x86_64-windows"
runnerTag Amd64 FreeBSD13 = "x86_64-freebsd13"

tags :: Arch -> Opsys -> [String]
tags arch opsys = [runnerTag arch opsys] -- Tag for which runners we can use

-- These names are used to find the docker image so they have to match what is
-- in the docker registry.
distroName :: LinuxDistro -> String
distroName Debian12  = "debian:12"
distroName (Debian11 {})  = "debian:11"
distroName (Debian10 {})  = "debian:10"
distroName Debian9   = "debian:9"
distroName Ubuntu2004 = "ubuntu:20.04"
distroName Ubuntu1804 = "ubuntu:18.04"
distroName Ubuntu2204 = "ubuntu:22.04"
distroName Ubuntu2404 = "ubuntu:24.04"
distroName Fedora33  = "fedora:33"
distroName Fedora27  = "fedora:27"
distroName Fedora36  = "fedora:36"
distroName Fedora38  = "fedora:38"
distroName (Fedora39 {})  = "fedora:39"
distroName Fedora40       = "fedora:40"
distroName (Alpine V3_12 _)  = "alpine:3.12"
distroName (Alpine V3_18 _) = "alpine:3.18"
distroName (Alpine V3_20 _)  = "alpine:3.20"
distroName ArchLinux   = "archlinux:latest"
distroName LinuxMint19 = "linuxmintd/mint19.3-amd64"
distroName LinuxMint20 = "linuxmintd/mint20.2-amd64"
distroName LinuxMint21 = "linuxmintd/mint21.3-amd64"


-- Discovered when Stretch got archived.
-- https://stackoverflow.com/questions/76094428/debian-stretch-repositories-404-not-found
useDebianArchive :: [String]
useDebianArchive =
    [ "sed -i s/deb.debian.org/archive.debian.org/g /etc/apt/sources.list"
    , "sed -i 's|security.debian.org|archive.debian.org/|g' /etc/apt/sources.list"
    , "sed -i /-updates/d /etc/apt/sources.list"
    ]

updateRepos :: Opsys -> [String]
updateRepos (Linux Debian9) = useDebianArchive
updateRepos (Linux _) = []
updateRepos Windows = []
updateRepos (Darwin {}) = []

installLlvmDebian :: LlvmVersion -> [String]
installLlvmDebian (LlvmVersion ver) =
  [ "wget https://apt.llvm.org/llvm.sh"
  , "chmod +x llvm.sh"
  , "./llvm.sh " ++ show ver
  ]

data ToolReqs = ToolReqs { installCmd :: String
                         , toolReqs   :: [String] }

toolReqsScript ToolReqs{..} = unwords (installCmd : toolReqs)

debianTools linker = ToolReqs { installCmd = "apt-get update && apt-get install -y"
                       , toolReqs  = [ "lsb-release", "software-properties-common", "gnupg", "wget", "curl", "bash", "git", "build-essential", "curl", "libffi-dev",  "libgmp-dev", "libgmp10", "libncurses-dev", "libncurses5", "libtinfo5"] ++ debianLinker linker
                       }
  where
    debianLinker Default = []
    debianLinker NoOverride = []
    debianLinker Bfd = [ "build-essential" ]
    debianLinker Gold = [ "build-essential" ]
    debianLinker Lld = [ "lld" ]


data LibffiVersion = Libffi6 | Libffi7

ubuntuToolsBase :: ToolReqs
ubuntuToolsBase =
  ToolReqs { installCmd = "apt-get update && apt-get install -y"
           , toolReqs = ["bash", "git", "build-essential", "curl", "libffi-dev", "libgmp-dev", "libgmp10", "libncurses-dev"]
           }
-- Needed before 2404
ubuntuToolsExtra :: LibffiVersion -> ToolReqs
ubuntuToolsExtra libffiVersion = ubuntuToolsBase { toolReqs = toolReqs ubuntuToolsBase ++ [ libffi, "libncurses5", "libtinfo5" ] }
  where
    libffi = case libffiVersion of
              Libffi6 -> "libffi6"
              Libffi7 -> "libffi7"

fedoraTools linker = ToolReqs { installCmd = "dnf install -y"
                       , toolReqs   = ["bash", "git", "which", "gcc", "g++", "gmp", "gmp-devel", "make", "ncurses", "ncurses-compat-libs", "xz", "perl"] ++ fedoraLinker linker
                       }
  where
    fedoraLinker Mold = ["mold"]
    fedoraLinker _ = []

fedora27Tools = (fedoraTools Default) { toolReqs = ["which", "findutils", "gcc", "gcc-c++", "gmp", "gmp-devel", "make", "ncurses", "ncurses-compat-libs", "xz", "perl"] }

alpineTools = ToolReqs { installCmd = "apk update && apk add"
                       , toolReqs = [ "bash", "git", "curl", "binutils-gold", "gcc", "g++", "gmp-dev", "libc-dev", "libffi-dev", "make", "musl-dev", "ncurses-dev", "perl", "tar", "xz"]
                       }

archLinuxTools = ToolReqs { installCmd = "pacman -Syu --noconfirm"
                          , toolReqs = [ "bash", "git", "curl", "which", "gcc", "gmp", "libffi", "make", "ncurses", "perl", "tar", "xz"]
                          }

linuxMintTools libffiVersion =
  ToolReqs { installCmd = "apt-get update && apt-get install -y"
           , toolReqs = ["build-essential", "curl", "libffi-dev", libffi, "libgmp-dev", "libgmp10", "libncurses-dev", "libncurses5", "libtinfo5"]
           }
  where
    libffi = case libffiVersion of
              Libffi6 -> "libffi6"
              Libffi7 -> "libffi7"


distroTools Debian12 = debianTools Default
distroTools (Debian11 _) = debianTools Default
distroTools (Debian10 linker) = debianTools linker
distroTools Debian9  = debianTools Default
distroTools Ubuntu1804 = ubuntuToolsExtra Libffi6
distroTools Ubuntu2004 = ubuntuToolsExtra Libffi7
distroTools Ubuntu2204 = ubuntuToolsExtra Libffi7
distroTools Ubuntu2404 = ubuntuToolsBase
distroTools Fedora27 = fedora27Tools
distroTools Fedora33 = fedoraTools Default
distroTools Fedora36 = fedoraTools Default
distroTools Fedora38 = fedoraTools Default
distroTools (Fedora39 linker) = fedoraTools linker
distroTools Fedora40 = fedoraTools Default
distroTools Alpine {} = alpineTools
distroTools ArchLinux  = archLinuxTools
distroTools LinuxMint19 = linuxMintTools Libffi6
distroTools LinuxMint20 = linuxMintTools Libffi7
distroTools LinuxMint21 = linuxMintTools Libffi7

installToolsScript :: Opsys -> [String]
installToolsScript (Linux dist) =
  case dist of
    Debian11 mllvm -> concat $
      [ toolReqsScript (distroTools dist) ]
      :
      [ installLlvmDebian llvm | Just llvm <- [mllvm] ]
    _ -> [ toolReqsScript (distroTools dist) ]
installToolsScript (Windows) = []
installToolsScript (Darwin Brew) =
  [ ".gitlab/darwin/brew/run.sh llvm@16"
  , "export PATH=\"$HOME/.brew/bin:$HOME/.brew/opt/llvm@16/bin:$PATH\""
  , "export CC=\"$HOME/.brew/opt/llvm@16/bin/clang\""
  , "export CXX=\"$HOME/.brew/opt/llvm@16/bin/clang++\""
-- ld.lld doesn't work like ld linker.. if you configure this you also need to configure LDFLAGS to pass additional options
--  , "export LD=\"$HOME/.brew/opt/llvm@16/bin/ld.lld\""
  , "export AR=\"$HOME/.brew/opt/llvm@16/bin/llvm-ar\""
  , "export RANLIB=\"$HOME/.brew/opt/llvm@16/bin/llvm-ranlib\""
  -- LLVM toolchain
  , "export LLVMAS=\"$HOME/.brew/opt/llvm@16/bin/clang\""
  , "export OPT=\"$HOME/.brew/opt/llvm@16/bin/opt\""
  , "export LLC=\"$HOME/.brew/opt/llvm@16/bin/llc\""
  ]
installToolsScript _ = []

linkerName :: Linker -> Maybe String
linkerName Default = Nothing
linkerName NoOverride = Nothing
linkerName Mold = Just "mold"
linkerName Bfd =  Just "bfd"
linkerName Gold = Just "gold"
linkerName Lld  = Just "lld"

linkerJobTag :: Linker -> String
linkerJobTag Default = "default"
linkerJobTag NoOverride = "no-override"
linkerJobTag Mold =  "mold"
linkerJobTag Bfd  =  "bfd"
linkerJobTag Gold =  "gold"
linkerJobTag Lld  =  "lld"

distroExtra :: LinuxDistro -> Maybe String
distroExtra (Debian10 linker) = Just (linkerJobTag linker)
distroExtra (Fedora39 linker) = Just (linkerJobTag linker)
distroExtra (Alpine _ linker) = Just (linkerJobTag linker)
distroExtra (Debian11 llvm)   = (("llvm" ++) . show . llvmVersion) <$> llvm
distroExtra _ = Nothing

opsysName :: Opsys -> String
opsysName (Linux distro) = "linux-" ++ distroName distro
                            ++ concat [ "-" ++ extra | Just extra <- [distroExtra distro]]
opsysName (Darwin toolchain) = "darwin-" ++ toolchainName toolchain
opsysName FreeBSD13 = "freebsd13"
opsysName Windows = "windows"

archName :: Arch -> String
archName Amd64 = "x86_64"
archName AArch64 = "aarch64"
archName I386  = "i386"

toolchainName :: DarwinToolchain -> String
toolchainName Brew = "brew"
toolchainName System = "system"

-- The path to the docker image (just for linux builders)
dockerImage :: Arch -> Opsys -> Maybe String
dockerImage arch (Linux distro) =
    Just (archStr ++ image)
  where
    image = distroName distro
    archStr = case arch of
      I386 -> "i386/"
      _ -> ""
dockerImage _ _ = Nothing

-----------------------------------------------------------------------------
-- Platform specific variables
-----------------------------------------------------------------------------

-- The variables map is a monoidal map so that we don't ever accidentally lose
-- variables settings by silently overwriting when merging. At the end these variables
-- are combinated together with spaces if they are set multiple times. This may
-- produce nonsense but it's easier to debug that silently overwriting.
--
-- The "proper" solution would be to use a dependent monoidal map where each key specifies
-- the combination behaviour of it's values. Ie, whether setting it multiple times is an error
-- or they should be combined.
newtype MonoidalMap k v = MonoidalMap (Map k v)
    deriving (Eq, Show, Functor, ToJSON)

instance (Ord k, Semigroup v) => Semigroup (MonoidalMap k v) where
    (MonoidalMap a) <> (MonoidalMap b) = MonoidalMap (Map.unionWith (<>) a b)

instance (Ord k, Semigroup v) => Monoid (MonoidalMap k v) where
    mempty = MonoidalMap (Map.empty)

mminsertWith :: Ord k => (a -> a -> a) -> k -> a -> MonoidalMap k a -> MonoidalMap k a
mminsertWith f k v (MonoidalMap m) = MonoidalMap (Map.insertWith f k v m)

type Variables = MonoidalMap String [String]

(=:) :: String -> String -> Variables
a =: b = MonoidalMap (Map.singleton a [b])

distroArchSupportsLlvm :: Arch -> Opsys -> Bool
distroArchSupportsLlvm _ (Linux (Debian11 llvm)) = isJust llvm
distroArchSupportsLlvm _ (Darwin Brew) = True
distroArchSupportsLlvm _ _ = False

opsysVariables :: Arch -> Opsys -> Variables
opsysVariables arch (Linux distro) = distroArchVariables arch distro
opsysVariables AArch64 (Darwin {}) =
  mconcat [ "NIX_SYSTEM" =: "aarch64-darwin"
          ]
opsysVariables Amd64 (Darwin {}) =
  mconcat [ "NIX_SYSTEM" =: "x86_64-darwin"
          ]
opsysVariables _ Windows = mconcat [ "RELOCATABLE_BUILD" =: "1" ]
opsysVariables _ _ = mempty


distroArchVariables :: Arch -> LinuxDistro -> Variables
distroArchVariables Amd64 (Debian10 linker) = mconcat [
    mconcat [ "CONF_GCC_LINKER_OPTS_STAGE2" =: ("-fuse-ld=" ++ name) | Just name <- [linkerName linker] ]
  , mconcat [ "LD" =: path | Just path <- [linkerPath linker] ]
  , mconcat [ "CONFIGURE_ARGS" =: "--disable-ld-override" | passLdOverride ]
  ]
  where
    passLdOverride = isJust (linkerName linker) || linker == NoOverride

    linkerPath Default = Nothing
    linkerPath NoOverride = Nothing
    linkerPath Bfd = Just "/usr/bin/ld.bfd"
    linkerPath Gold = Just "/usr/bin/ld.gold"
    linkerPath Lld = Just "/usr/bin/ld.lld"
distroArchVariables Amd64 (Fedora39 linker) = mconcat [
      mconcat [ "CONF_GCC_LINKER_OPTS_STAGE2" =: ("-fuse-ld=" ++ name) | Just name <- [linkerName linker] ]
    , mconcat [ "LD" =: path | Just path <- [linkerPath linker] ]
    , mconcat [ "CONFIGURE_ARGS" =: "--disable-ld-override" | passLdOverride ]
  ]
  where
    passLdOverride = isJust (linkerName linker) || linker == NoOverride
    linkerPath Mold = Just "/usr/bin/ld.mold"
    linkerPath Default = Nothing
distroArchVariables _ (Alpine _ linker) =
  mconcat [ "CONFIGURE_ARGS" =: "--disable-ld-override" | passLdOverride ]
  where
    passLdOverride = isJust (linkerName linker) || linker == NoOverride
distroArchVariables _ _ = mempty

-----------------------------------------------------------------------------
-- Artifacts, what to store and how long for
-----------------------------------------------------------------------------

-- | A 'Job' is the description of a single job in a gitlab pipeline. The
-- job contains all the information about how to do the build but can be further
-- modified with information about when to run jobs, which variables to set for
-- certain platforms and so on.
data Job
  = Job { jobStage :: String
        , jobNeeds :: [String]
        , jobTags :: [String]
        , jobAllowFailure :: Bool
        , jobScript :: [String]
        , jobAfterScript :: [String]
        , jobDockerImage :: Maybe String
        , jobVariables :: Variables
        , jobDependencies :: [String]
        , jobRules :: [SimpleRule]
        }

data SimpleRule = SimpleRule String String

instance ToJSON SimpleRule where
  toJSON (SimpleRule cond w) = object ([
    "if" A..= toJSON cond
    , "when" A..= toJSON w ])

instance ToJSON Job where
  toJSON Job{..} = object
    [ "stage" A..= jobStage
    , "needs" A..= map (\j -> object [ "job" A..= j, "artifacts" A..= True ]) jobNeeds
    , "dependencies" A..= jobDependencies
    , "image" A..= jobDockerImage
    , "tags" A..= jobTags
    , "allow_failure" A..= jobAllowFailure
    -- Joining up variables like this may well be the wrong thing to do but
    -- at least it doesn't lose information silently by overriding.
    , "variables" A..= fmap (intercalate " ") jobVariables
    , "after_script" A..= jobAfterScript
    , "script" A..= jobScript
    , "rules" A..= jobRules
    ]

mkJobName :: JobType -> Arch -> Opsys -> String
mkJobName jt arch os = jtName jt ++ "-" ++ archName arch ++ "-" ++ opsysName os

jtName DirectInstall = "direct"
jtName GhcupInstall = "ghcup"


data JobType = DirectInstall | GhcupInstall deriving Eq

-- | Build a job description from the system description and 'BuildConfig'
job :: JobType -> Arch -> Opsys -> (String, Job)
job jt arch opsys = (jobName, Job {..})
  where
    jobName = mkJobName jt arch opsys

    jobTags = tags arch opsys

    jobDockerImage = dockerImage arch opsys

    jobScript
      = updateRepos opsys <>
        installToolsScript opsys <>
        [
          (if opsys == Windows then "bash " else "") ++  ".gitlab/install-bindist.sh ghc"
        ]

    jobAfterScript = []

    jobDependencies = ["setup-env"]
    jobVariables = mconcat
      [ opsysVariables arch opsys
      , mconcat [ "TEST_LLVM" =: "1" | distroArchSupportsLlvm arch opsys ]
      , mconcat [ "BINDIST_ONLY" =: "1" | jt == DirectInstall ]
      ]


    jobAllowFailure = False
    jobStage = "full-build"
    jobNeeds = ["setup-env"]
    jobRules = [ SimpleRule "$ONLY_JOB == null" "always"
               , SimpleRule ("$ONLY_JOB == \"" ++ jobName ++ "\"") "always" ]

---------------------------------------------------------------------------
-- Job Modifiers
---------------------------------------------------------------------------

-- Building the standard jobs

data JobGroup = JobGroup { direct :: (String, Job), ghcup :: (String, Job) }

flattenJobGroup :: JobGroup -> [(String, Job)]
flattenJobGroup (JobGroup a1 a2) = [a1, a2]

modifyDirectJob f (JobGroup a1 a2) = JobGroup (f a1) a2

modifyGhcupJob f (JobGroup a1 a2) = JobGroup a1 (f a2)

modifyJobGroup f = modifyDirectJob f . modifyGhcupJob f

standardBuilds :: Arch -> Opsys -> JobGroup
standardBuilds arch opsys = JobGroup (job DirectInstall arch opsys) (job GhcupInstall arch opsys)

allowFailure :: (String, Job) -> (String,Job)
allowFailure (s,j) = (s, j { jobAllowFailure = True } )

brokenAlpineConfiguration v l =
  case (v, l) of
    (V3_12, Default) -> True
    (V3_18, Default) -> True
    _ -> False


-- | Specification for all the jobs we want to build.
jobs :: Map String Job
jobs = Map.fromListWithKey (\k _ _ -> error ("Duplicate job name: " ++ k))  $ concatMap flattenJobGroup $
     [ standardBuilds Amd64 (Linux (Debian11 Nothing))
     , standardBuilds Amd64 (Linux Debian12)
     , standardBuilds Amd64 (Linux Debian9)
     , standardBuilds Amd64 (Linux Fedora27)
     , standardBuilds Amd64 (Linux Fedora33)
     , standardBuilds Amd64 (Linux Fedora36)
     , standardBuilds Amd64 (Linux Fedora38)
     , standardBuilds Amd64 (Linux Fedora40)
     ]
     ++
     [ standardBuilds Amd64 (Linux (Debian10 l)) | l <- [NoOverride, Default, Bfd, Gold, Lld]]
     ++
     [standardBuilds Amd64 (Linux (Fedora39 l)) | l <- [Default, Mold]]
     ++
     [
       standardBuilds Amd64 (Linux Ubuntu2004)
     , standardBuilds Amd64 (Linux Ubuntu2204)
     , standardBuilds Amd64 (Linux Ubuntu2404)
     , standardBuilds Amd64 (Linux Ubuntu1804) ]
     ++
     [ mark_failure (standardBuilds Amd64 (Linux (Alpine v l)))
     | v <- [V3_12, V3_18, V3_20]
     , l <- [Default,  NoOverride ]
     , let mark_failure = if brokenAlpineConfiguration v l then modifyDirectJob allowFailure else id ]
     ++
     [ standardBuilds Amd64 (Linux ArchLinux)
     , standardBuilds Amd64 (Linux LinuxMint19)
     , standardBuilds Amd64 (Linux LinuxMint20)
     , standardBuilds Amd64 (Linux LinuxMint21)
     , standardBuilds AArch64 (Linux (Debian10 Default)) ]
     ++
     [ standardBuilds AArch64 (Linux (Debian11 (Just llvm)) ) | llvm <- supportedLlvmVersions ]
     ++
     [
       standardBuilds AArch64 (Linux Debian12)
     , standardBuilds AArch64 (Linux Ubuntu2204)
     , standardBuilds AArch64 (Linux Ubuntu2404)
     , standardBuilds AArch64 (Linux Ubuntu2004)
     -- No ghcup binaries for aarch64 alpine yet
     , modifyJobGroup allowFailure (standardBuilds AArch64 (Linux (Alpine V3_18 Default)))
     , modifyJobGroup allowFailure (standardBuilds AArch64 (Linux (Alpine V3_20 Default)))
     , standardBuilds I386 (Linux (Debian10 Default))
     , standardBuilds I386 (Linux (Debian11 Nothing))
     -- Seems that something is broken but not exactly clear what
     -- https://github.com/haskell/ghcup-hs/issues/1107
     --
     , modifyJobGroup allowFailure $ standardBuilds I386 (Linux Debian12)
     , standardBuilds Amd64 Windows
     , standardBuilds Amd64 (Darwin System)
     , standardBuilds AArch64 (Darwin System)
     , standardBuilds AArch64 (Darwin Brew)
     ]

main = do
  as <- getArgs
  (case as of
    [] -> B.putStrLn
    (fp:_) -> B.writeFile fp)
    (A.encode jobs)
