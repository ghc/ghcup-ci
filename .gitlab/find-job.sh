#!/usr/bin/env bash

set -e

project_id=$1
pipeline_id=$2
job_name=$3

if [ -z "$project_id" ]; then
  echo "Missing project_id (1)"
  exit 1
fi

if [ -z "$pipeline_id" ]; then
  echo "Missing pipeline_id (1)"
  exit 1
fi

if [ -z "$job_name" ]; then
  echo "Missing job_name (1)"
  exit 1
fi

resp=$(mktemp)

# Access token is a protected environment variable in the head.hackage project and
# is necessary for this query to succeed. Sadly job tokens only seem to
# give us access to the project being built.
curl \
  --silent --show-error \
  -H "Private-Token: $ACCESS_TOKEN" \
  "https://gitlab.haskell.org/api/v4/projects/$project_id/pipelines/$pipeline_id/jobs?scope[]=success&per_page=50" \
  > "$resp"

job_id=$(jq ". | map(select(.name == \"$job_name\")) | .[0].id" < "$resp")
if [ "$job_id" = "null" ]; then
  echo "Error finding job $job_name for $pipeline_id in project $project_id:" >&2
  cat "$resp" >&2
  rm "$resp"
  exit 1
else
  rm "$resp"
  echo -n "$job_id"
fi
