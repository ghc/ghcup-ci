#!/usr/bin/env bash
set -x
set -eo pipefail

runner_temp=$(mktemp -d)

if [ -z "$BINDIST_URL" ]; then
  cat build.env
  source build.env
fi

# See GHC #24951 for a user with a misconfigured CDPATH variable
cdpath_temp=$(mktemp -d)
mkdir -p $cdpath_temp/lib
mkdir -p $cdpath_temp/wrappers
mkdir -p $cdpath_temp/doc
mkdir -p $cdpath_temp/share
export CDPATH=$cdpath_temp:.

export GHCUP_INSTALL_BASE_PREFIX=$runner_temp/foobarbaz

export BOOTSTRAP_HASKELL_NONINTERACTIVE=1
export BOOTSTRAP_HASKELL_MINIMAL=1
export BOOTSTRAP_HASKELL_ADJUST_BASHRC=1

curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh

source $GHCUP_INSTALL_BASE_PREFIX/.ghcup/env || source ~/.bashrc

ghcup --version
which ghcup | grep foobarbaz

# Testing a manual installation
if [ -n "$BINDIST_ONLY" ]; then
  install_dir="$runner_temp/ghc"
  install_temp=$(mktemp -d)
  download_temp=$(mktemp -d)
  pushd $download_temp
  if [ -n "$BINDIST_URL" ]; then
    wget "$BINDIST_URL"
  else
    ghcup --metadata-caching=0 --url-source=$METADATA_URI -v prefetch ghc $VERSION -d "$download_temp"
  fi
  tar -xf $(find . -name *\.tar*) -C "$install_temp" ||
    (unzip $(find . -name *\.zip*) -d "$install_temp")
  popd
  if [ -n "$RELOCATABLE_BUILD" ]; then
    cp -r "$install_temp/ghc-"* "$install_dir"
  else
    pushd $install_temp
    pushd "ghc-"*
    ./configure --prefix=$install_dir $CONFIGURE_ARGS
    make install
    popd
    popd
  fi
  export PATH=$install_dir/bin:$PATH

# Testing an installation via ghcup
else
  if [ -n "$BINDIST_URL" ]; then
    ghcup --metadata-caching=0 -v install ghc -u $BINDIST_URL --set custom-bindist -- $CONFIGURE_ARGS || (cat config.log && exit 1)
  else
    ghcup --metadata-caching=0 -v --url-source=$METADATA_URI install ghc --set $VERSION -- $CONFIGURE_ARGS || (cat config.log && exit 1)
  fi
fi


ghcup --metadata-caching=0 -v install cabal latest

mkdir $runner_temp/install-bindist-ci
cp -r test-projects $runner_temp/install-bindist-ci
cd $runner_temp/install-bindist-ci


ghc --version
ghc --info

ghc-pkg check

cabal update
cabal get acme-box-0.0.0.0
(cd acme-box-0.0.0.0/; ghc Setup.hs)
(cd acme-box-0.0.0.0/; ./Setup configure)
(cd acme-box-0.0.0.0/; ./Setup build)

cat <<EOF > main.hs
{- cabal:
build-depends: base
-}

main = print $ 1 + 1
EOF

# Use Setup.hs to build acme-box

# Nightly bindists don't have profiling libraries
if [ -n "$NO_PROFILING" ]; then
  ghc main.hs
else
  ghc -prof main.hs
fi
[[ $(./main +RTS -s) -eq 2 ]]


cabal install --lib --package-env=$(mktemp) clock
cabal install --lib --package-env=$(mktemp) hashable --allow-newer=ghc-bignum


case "$(uname)" in
  MSYS_*|MINGW*) echo "skipping dynamic test" ;;
  *) (cd test-projects/merge-objects; cabal run merge-objects --enable-executable-dynamic) ;;
esac
(cd test-projects/merge-objects; cabal run merge-objects)

if [ -n "$TEST_LLVM" ]; then
  ghc -fllvm main.hs -fforce-recomp
  [[ $(./main +RTS -s) -eq 2 ]]
fi

HADDOCK_HTML_INDEX="$(ghc-pkg field base haddock-html --simple)/index.html"
# Checking that haddock-htmls is populated correctly
if [ -f $HADDOCK_HTML_INDEX ]
then
  ls -alh $HADDOCK_HTML_INDEX
else
  echo "Haddock html field incorrect path" && exit 1
fi
