#!/usr/bin/env bash

set -e

usage() {
  echo "$0 (job-name) (commit) (bindist_url)"
  exit 1
}

if [[ -z "$JOB_NAME" ]]; then
  JOB_NAME="$1"
  shift
fi
if [[ -z "$JOB_NAME" ]]; then
  usage
fi

COMMIT="$1"
if [[ -z "$1" ]]; then
  COMMIT="master"
else
  COMMIT="$1"
  shift
fi

if [[ -n "$1" ]]; then
  BINDIST_URL="$1"
  shift
fi

echo "COMMIT: $COMMIT"
echo "JOB_NAME: $JOB_NAME"
echo "BINDIST_URL: $BINDIST_URL"

jobs_yaml=$(curl "https://gitlab.haskell.org/ghc/ghcup-ci/-/raw/$COMMIT/.gitlab/jobs.yaml")

echo "$jobs_yaml"

sel() {
  echo "$jobs_yaml" | tail -n +2 | jq -r ".\"$JOB_NAME\"$1"
}

IMAGE=$(sel ".image")
echo ${IMAGE}

VARIABLES=$(sel ".variables | to_entries | .[] | select(.value | . != null and . != \"\") | \"ENV \(.key) \(.value)\"")
echo "$VARIABLES"

if [ -n "$BINDIST_URL" ]; then
BINDIST_URL_SET="ENV BINDIST_URL $BINDIST_URL"
else
BINDIST_URL_SET=""
fi

if [ -n "$GITLAB_TOKEN" ]; then
REPO="https://$GITLAB_TOKEN@gitlab.haskell.org/ghc/ghcup-ci.git"
else
REPO="https://gitlab.haskell.org/ghc/ghcup-ci.git"
fi


RUN_SCRIPT=$(sel ".script | join(\" && \")")
echo $RUN_SCRIPT

tmp="$(mktemp -d)"
mkdir -p "$tmp"

cat >"$tmp/run_script" <<EOF
#! /usr/bin/env bash

set -e

cp default.env build.env

$(sel ".script | join (\" \n\")")
EOF

cat "$tmp/run_script"

cat >"$tmp/Dockerfile" <<EOF
FROM $IMAGE
RUN if which apt-get; then \
      apt-get update && apt-get install -y sudo git vim htop tmux gdb strace; \
    elif which apk; then \
      apk add sudo git vim htop tmux gdb strace; \
    elif which dnf; then \
      sudo dnf install -y vim htop tmux gdb strace; \
    fi

RUN git config --global user.email "$(git config --global --get user.email)" && \
    git config --global user.name "$(git config --global --get user.name)"
RUN git config --global http.version HTTP/1.1
RUN mkdir -p ghcup-ci && cd ghcup-ci && git init
RUN cd ghcup-ci && git remote add origin $REPO
RUN cd ghcup-ci && git fetch --depth 1 origin $COMMIT && git checkout $COMMIT && \
    git submodule update --init
WORKDIR /ghcup-ci
$VARIABLES
$BINDIST_URL_SET
ENV CPUS 6
COPY "\$tmp/run_script" .
RUN sudo chmod +x run_script
CMD bash -c 'while true; do sleep 60; done;'
EOF

iidfile="$(mktemp)"
rm $iidfile
docker build --no-cache --iidfile="$iidfile" "$tmp"
image="$(cat "$iidfile")"
rm -f "$iidfile"
#rm -R "$tmp"
echo "Development environment image is:"
echo "  $image"

cidfile="$(mktemp -u)"
docker create --cidfile="$cidfile" "$image"
container="$(cat "$cidfile")"
rm -f "$cidfile"
echo
echo "Development environment container is:"
echo "  $container"
echo
echo "To start another shell in container run:"
echo
echo "    docker exec -it $container bash -i"
echo

docker start "$container"
docker exec -it "$container" bash -i
echo
echo "To drop container run:"
echo
echo "    docker container rm --force --volumes $container"
echo
