# ghcup testing infrastructure

This repo is used to test metadata produced by GHC used

There are three ways to setup the environment.

1. Using the defaults in the repo.
2. Explicitly trigger a job with the relevant variables
3. Trigger a job from upstream by passing `$UPSTREAM_PIPELINE_ID` (this is the method that
   ghc uses to trigger a testing job here)

## Relevant environment variables for manual triggering

* `METADATA_URI` - The URI for the metadata to test
* `VERSION` - The version of GHC you wish to test installing
* `NO_PROFILING` - Set to 1 when there are no profiling libraries.
* `BINDIST_ONLY` - Install using make/install rather than using ghcup
* `BINDIST_URL`  - Install a specific bindist (only works when triggering one build)
* `RELOCATABLE_BUILD`  - Install a relocatable bindist (mostly used on windows)

## Relevant environment variables for upstream trigger

* `UPSTREAM_PIPELINE_ID`  - The pipeline ID which the generated metadata lives in
* `UPSTREAM_PROJECT_ID`   - The project ID for the upstream project (almost always `1` (for ghc/ghc))
* `UPSTREAM_JOB_NAME`     - The job which the metadata belongs to (ie `ghcup-metadata-nightly`)
* `UPSTREAM_PROJECT_PATH` - The path of the upstream project (almost always ghc/ghc)

## Triggering only a specific job

If you are debugging something on a particular platform then you can run one specific
job with by setting the `ONLY_JOB` environment variable with the name of the job you want to run.

For example, via the CLI.

```
git push origin wip/direct-install -o ci.variable="ONLY_JOB=direct-x86_64-windows"
```

You can also set it via the web interface.

This can be used in conjunction with `BINDIST_URL` and the other configuration options.

## Running jobs locally

The `./ghcup-ci-docker.sh` script can be used to run jobs locally.

```
./ghcup-ci-docker.sh <job_name> <optional:commit> <optional:bindist_url>
```

The default is to start a docker container which you can run the CI script in by
running


```
./run_script
```

The default commit is `master` and the default configuration is specified by `default.env`.

If you just want to test one bindist for a specific platform then you can pass the
bindist URL directly to the script as the third argument.

In addition you can set the `$GITLAB_TOKEN` environment variable if you want to be
able to push to the repo from witin the docker container.


